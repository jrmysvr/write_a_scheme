module Main where

import Text.ParserCombinators.Parsec hiding (spaces)
import System.Environment
import Control.Monad

data LispVal = Atom String                   -- Stores a String naming the atom
             | List [LispVal]                -- List of other LispVals
             | DottedList [LispVal] LispVal  -- `Improper` List - List of all elements except last, last is a another field
             | Number Integer                
             | String String                 
             | Bool Bool
             
spaces :: Parser ()
spaces = skipMany1 space

symbol :: Parser Char
symbol = oneOf "!#$%@^&*<>+_-=~|/?"

--readExpr input = case parse (spaces >> symbol) "lisp" input of
readExpr :: String -> String
readExpr input = case parse parseExpr "lisp" input of 
  Left err -> "No Match: " ++ show err 
  Right val -> "Found value"

parseAtom :: Parser LispVal
parseAtom = do 
              -- return $ Atom []
              first <- letter <|> symbol
              rest <- many (letter <|> digit <|> symbol)
              let atom = first:rest
              return $ case atom of 
                        "#t" -> Bool True
                        "#f" -> Bool False
                        _    -> Atom atom

parseString :: Parser LispVal
parseString = do
                char '"'
---- Exercise 2: Update Strings for R5RS compliance
-- Not yet implemented
---- Exercise 3: Update Strings for whitespace characters (\n \r \t \\)
-- Not yet implemented
                x <- many (noneOf "\"")
                char '"'
                return $ String x

parseNumber :: Parser LispVal
-- parseNumber = liftM (Number . read) $ many1 digit

---- Exercises 1: Rewrite without `liftM`
---- do notation
-- parseNumber = do
--                 digits <- many1 digit
--                 return $ (Number . read) digits

---- >>= operator
-- parseNumber = (many1 digit) >>= (\x -> return $ (Number . read) x)
parseNumber = (many1 digit) >>= return . Number . read

parseExpr :: Parser LispVal
parseExpr = parseAtom
        <|> parseString
        <|> parseNumber
-- parseExpr = parseNumber
-- parseExpr = parseAtom


main :: IO()
main = do
  (expr: _) <- getArgs
  putStrLn (readExpr expr)