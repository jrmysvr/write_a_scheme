# [Write Yourself a Scheme](https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours)

Install/Build Project

> cabal install --only-dependencies

> cabal build

Run

> cabal run *args*

After building

> scheme.bat *args*



### Currently working on [Parsing](https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours/Parsing)


---

## Answers to exercises: https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours/Answers
